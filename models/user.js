

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Username is required"],
		unique: true	
	},
	email : {
		type : String,
		required : [true, "Email is required"],
		unique: true
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
		
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	}
	},
	{ timestamps: true}

);



module.exports = mongoose.model("User", userSchema);	